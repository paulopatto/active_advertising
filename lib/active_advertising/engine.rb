module ActiveAdvertising
  class Engine < ::Rails::Engine
    isolate_namespace ActiveAdvertising
  end
end
